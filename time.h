//Header file for Time class
//2018-02-01
//Andrew Patterson
//cs142

#include <iostream>
//#pragma once

#ifndef TIME_H
#define TIME_H

class Time
{
public:
	//Enum, have 0 issues w/ other things reading it
	enum Conversions {HOURS_PER_DAY = 24, MINUTES_PER_HOUR = 60, SECONDS_PER_MINUTE = 60};

	//Printing function
	void print(std::ostream& out = std::cout) const;

	//Reading function I didn't realize was supposed to be part of time instead of just spitting one out
	void read(std::istream& in = std::cin);
	
	//Access functions
	int seconds() const			{ return seconds_; }
	int minutes() const			{ return minutes_; }
	int hours() const			{ return hours_; }
	int isTwelveHour() const	{ return isTwelveHour_;}

	//Constructors
	Time(int h = 0, int m = 0, int s = 0, bool is12h = true, bool isPM = false);			//default ctor and others
	Time(const Time& otherTime) : hours_(otherTime.hours_), minutes_(otherTime.minutes_),	//Copy ctor
		seconds_(otherTime.seconds_), isTwelveHour_(otherTime.isTwelveHour_) {}

	//Useful setting functions
	void incrementTime(unsigned int numSeconds = 1);
	bool set12hour(bool set) { isTwelveHour_ = set; return isTwelveHour_; }

	//operators
	Time operator- 	(const Time& other);
	Time operator+ 	(const Time& other);
	//void operator= 	(const Time& other);
	bool operator== (const Time& other);

private:
	int hours_;
	int minutes_;
	int seconds_;
	bool isTwelveHour_;

	void normalize();
};

Time currentTime();

Time localTime();

Time readTime(std::istream& in = std::cin);

#endif

//Time class - 2018-01-29, -01-31 classwork; Lab 3
//Andrew Patterson
//cs142

#include <iostream>
#include <ctime>
#include <cassert>
#include "time.h"

int main()
{
	//Testing ctor basics
	Time constructorHours(11);
	assert(constructorHours == Time(11, 0, 0));
	std::cout << "Ctor with less than all things works\n";

	//Testing current time
	Time realTimeSeconds = currentTime();
	std::cout << "This should be the current time (ignoring DST)\n";
	realTimeSeconds.print();
	std::cout << "\n\n";

	//I made a function that takes the time from localtime().
	//DON'T test this during DST without a few changes
	Time local = localTime();
	assert(realTimeSeconds == local);

	//Testing the default ctor (and printing of 0 values)
	Time defaultCtor;
	assert(defaultCtor == Time(0,0,0));
	std::cout << "This should be 12:00:00 (bcs 12hr)\n";
	defaultCtor.print();
	std::cout << "\n\n";

	//Testing copying
	Time copy1(3, 4, 5);
	Time copy2 = copy1;
	assert(copy1 == copy2);
	std::cout << "Copy ctor works\n\n";

	//Testing readTime
	std::cout << "Type in a time: ";
	Time readTest = readTime();
	std::cout << "This should be the same as what you just put in.\n";
	readTest.print();
	std::cout << "\n\n";

	//Testing subtraction
	Time differenceTest(1, 2, 3);
	assert((differenceTest - defaultCtor) == differenceTest);
	std::cout << "Time difference works\n\n";

	//Testing addition
	Time sumTest(2, 3, 58);
	assert((sumTest + differenceTest) == Time(3, 6, 1));
	std::cout << "Sum works!\n\n";

	//Testing PM flag and 24 hour time printing
	Time twelve24test(2, 30, 0, true, true);
	assert(twelve24test == Time(14, 30));
	std::cout << "PM flag works\nHere's the 12 hour time:\n";
	twelve24test.print();
	std::cout << "\n\n";
	twelve24test.set12hour(false);
	std::cout << "This should be in 24 hour time\n";
	twelve24test.print();
	std::cout << "\n\n";

	//Testing normalization
	Time normalizeTest(0, 0, 9000);
	assert(normalizeTest == Time(2,30));
	std::cout << "Normalize works!\n";

	system("pause");
	return 0;
}

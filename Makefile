# Basic Makefile, as in my CS 14{1,2} txtbook
# (Thanks, Briggs!)

# Change to match actual program doing
PROG	:= time-class-basics

SRCS	:= $(wildcard *.cpp)
OBJS	:= ${SRCS:.cpp=.o}

$(PROG):	$(OBJS)
			$(CXX) -g -o $@ $^

%.o:		%.cpp
			$(CXX) -c $<

clean:
			rm -f $(PROG)
			rm -f $(OBJS)

make.depend:	$(SRCS)
			$(CXX) -MM $^ > $@

-include make.depend

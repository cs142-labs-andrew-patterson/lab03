//Functions for Time class
//2017-02-01
//Andrew Patterson
//cs142

#include <iostream>
#include <ctime>
#include <cmath>
#include "time.h"
#pragma warning (disable:4996)

void Time::print(std::ostream& out) const
{
	int printHour = hours_;
	if (isTwelveHour_)
	{
		printHour %= 12;
		if (printHour == 0) printHour = 12;
	}

	if (printHour < 10)
	{
		out << '0';
	}
	out << printHour << ':';

	if (minutes_ < 10)
	{
		out << '0';
	}
	out << minutes_ << ':';

	if (seconds_ < 10)
	{
		out << '0';
	}
	out << seconds_;

	if (isTwelveHour_ && (hours_ >= 12))
	{
		std::cout << " PM";
	}
}

void Time::read(std::istream& in) 
{
	char stringIn[8];		//Make a string long enought to hold a whole time value

	std::cin >> stringIn;
	char values[3][3] = { "00" };	//An array to hold 3 strings (hours, minutes, seconds), starting at 0

	int position = 0;

	for (int i = 0; i < 3; i++)		//Loop through all the positions
	{
		int j = 0;					//Always start with the first position of the string
		while (*(stringIn + position) != ':' && *stringIn != '\0' && j < 3)
		{
			values[i][j] = *(stringIn + position);
			++j;
			++position;
		}

		if (j < 2)
		{
			values[i][1] = values[i][0];
			values[i][0] = '0';
		}


		if (stringIn[position] == '\0')
		{
			break;					//Stop if hit the \0 at the end
		}

		++position;					//Increment position past the :
	}

	hours_ = atoi(values[0]);
	minutes_ = atoi(values[1]);
	seconds_ = atoi(values[2]);

	Time readTime(atoi(values[0]), atoi(values[1]), atoi(values[2]));
}

Time::Time(int hours, int minutes, int seconds, bool is12h, bool isPM) :
	hours_(hours),
	minutes_(minutes),
	seconds_(seconds),
	isTwelveHour_(is12h)
{
	if (isPM)
		hours_ += 12;

	normalize();
}

void Time::incrementTime(unsigned int numSeconds)
{
	seconds_ += numSeconds;
	normalize();
}

void Time::normalize()
{
	if (seconds() < 0)
	{
		minutes_ -= (abs(seconds_) / SECONDS_PER_MINUTE) + 1;
		seconds_ += (abs(seconds_) / SECONDS_PER_MINUTE) + 1;
	}

	if (seconds_ >= SECONDS_PER_MINUTE)
	{
		minutes_ += seconds_ / SECONDS_PER_MINUTE;
		seconds_ %= SECONDS_PER_MINUTE;
	}

	if (minutes_ < 0)
	{
		hours_  -= (abs(seconds_) / MINUTES_PER_HOUR) + 1;
		minutes_ += (abs(seconds_) / MINUTES_PER_HOUR) + 1;
	}

	if (minutes_ >= MINUTES_PER_HOUR)
	{
		hours_ += minutes_ / MINUTES_PER_HOUR;
		minutes_ %= MINUTES_PER_HOUR;
	}

	if (hours_ < 0)
	{
		hours_ += (abs(hours_) / HOURS_PER_DAY) + 1;
	}

	if (hours_ >= HOURS_PER_DAY)
	{
		hours_ %= HOURS_PER_DAY;
	}
}

Time currentTime()
{
	static const int TIME_ZONE = -5;

	int seconds = time(NULL);
	int minutes = seconds / 60;
	int hours = minutes / 60;
	hours += TIME_ZONE;

	Time now(hours % 24, minutes % 60, seconds % 60);

	return now;
}

Time localTime()
{
	time_t rawtime;
	struct tm * localeTime;

	time(&rawtime);
	localeTime = localtime(&rawtime);

	Time now(localeTime->tm_hour, localeTime->tm_min, localeTime->tm_sec);

	return now;
}

Time readTime(std::istream& in) //TODO: maybe make buffer-overflow-free?
{
	char stringIn[8];		//Make a string long enought to hold a whole time value

	std::cin >> stringIn;
	char values[3][3] = { "00" };	//An array to hold 3 strings (hours, minutes, seconds), starting at 0

	int position = 0;

	for (int i = 0; i < 3; i++)		//Loop through all the positions
	{
		int j = 0;					//Always start with the first position of the string
		while (*(stringIn + position) != ':' && *stringIn != '\0' && j < 3)
		{
			values[i][j] = *(stringIn + position);
			++j;
			++position;
		}

		if (j < 2)
		{
			values[i][1] = values[i][0];
			values[i][0] = '0';
		}


		if (stringIn[position] == '\0')
		{
			break;					//Stop if hit the \0 at the end
		}

		++position;					//Increment position past the :
	}

	Time readTime(atoi(values[0]), atoi(values[1]), atoi(values[2]));

	return readTime;
}


Time Time::operator- (const Time& other)
{
	int hourDiff = hours_ - other.hours_;
	int minuteDiff = minutes_ - other.minutes_;
	int secondDiff = seconds_ - other.seconds_;

	Time difference(hourDiff, minuteDiff, secondDiff);
	return difference;
}

Time Time::operator+ (const Time& other)
{
	int hourSum = hours_ + other.hours_;
	int minuteSum = minutes_ + other.minutes_;
	int secondSum = seconds_ + other.seconds_;

	Time sum(hourSum, minuteSum, secondSum);
	return sum;
}

bool Time::operator== (const Time& other)
{
	return ((hours_ == other.hours_) && (minutes_ == other.minutes_) && (seconds_ == other.seconds_));
}
